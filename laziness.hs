{-
 - laziness: a tool to memoize your Earth-seizure plans
 - by Sergey 'L29Ah' Alirzaev, <zl29ah@gmail.com>
 -
 - File format:
 - <id>\t<description>\t<importance>\t<start timestamp>\t<end timestamp>
 - [\t<dependency>[\t<dependency>...]]\n<id>\t...
 -}

import qualified Data.Map as M
import Data.List.Split
import Data.Maybe
import Data.DateTime

import Control.Monad

import System

data Todo = Todo {
	descr :: String,
	deps :: [String],	-- deps also can be used as anti-tags
	importance :: Integer,
	timeStart :: Integer,
	timeEnd :: Integer
} deriving (Show, Eq)

instance Ord Todo where
	compare x y = compare (importance x) (importance y)

parseLine :: String -> Maybe (String, Todo)
parseLine line | null line = Nothing
               | otherwise = Just $
                  (\(key:descr:imp:ts:te:deps) -> (key, Todo descr deps (read imp) (read ts) (read te))) $ sepBy "\t" line

prettyTodo :: Todo -> String
prettyTodo (Todo descr deps imp ts te) = "\27[" ++ (
	if imp > 100 then "31" else	-- Nice coloring
	if imp > 10 then "33" else
	if imp > -10 then "37" else
	if imp > -100 then "36" else "34") ++ "m" ++ descr ++ d ts ++ d te ++ "\n"
		where d x = if x == 0 then "" else " - " ++ prettyDate x

prettyDate :: Integer -> String
prettyDate = show . fromSeconds 

prettyLine :: Int -> Todo -> String
prettyLine depth x = "\27[37m" ++ padding ++ prettyTodo x
	where padding = concat $ replicate depth "|  "

pretty :: [(Int, Todo)] -> String
pretty = concatMap (uncurry prettyLine) 

dig :: M.Map String Todo -> String -> Int -> String
dig db id maxdepth = pretty $ _dig db id 0 maxdepth

_dig :: M.Map String Todo -> String -> Int -> Int -> [(Int, Todo)]
_dig _ _ _ 0 = []
_dig db id curdepth maxdepth = (curdepth, node) : 
	(concatMap (\x -> _dig db x (curdepth + 1) (maxdepth - 1)) $ deps node)
      			where node = fromMaybe (error $ "No node named \"" ++ id ++ "\" in the database!") $ M.lookup id db

-- TODO: use getOpt
main = do
	args <- getArgs
	when (length args == 0) $ error usage
	let (action:cmdargs) = args
	f <- readFile ".todo"
	let db = M.fromList $ catMaybes $ map parseLine $ lines f
	putStr $ case action of
		"dig" -> dig db (cmdargs!!0) (read $ cmdargs!!1)
		otherwise -> error usage

usage = "Usage:\n" ++
        "laziness dig <id> <depth> -- make laziness print todo graph starting at 'id' up to 'depth' jumps into\n"

